import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'data/crash_repository.dart';
import 'bloc/bloc.dart';
import 'pages/intro.dart';

void main() => runApp(MyApp());
