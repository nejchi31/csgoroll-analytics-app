import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:csgoroll_insights_app/data/roll_repository.dart';
import 'bloc.dart';

class RollBloc extends Bloc<RollEvent, RollState> {
  final RollRepository rollRepository;

  RollBloc(this.rollRepository) : super(null);

  @override
  RollState get initialState => RollInitial();

  @override
  Stream<RollState> mapEventToState(
    RollEvent event,
  ) async* {
    yield RollLoading();
    if (event is GetRoll) {
      try {
        final roll = await rollRepository.fetchRollAll();
        yield RollLoaded(roll);
      } on NetworkError {
        yield RollError("Couldn't fetch Roll. Is the device online?");
      }
    } else if (event is GetDetailedRoll) {
      try {
        final roll = await rollRepository.fetchRollLast(event.method, event.dFrom, event.dTo);
        yield RollLoaded(roll);
      } on NetworkError {
        yield RollError("Couldn't fetch Roll. Is the device online?");
      }
    }
  }
}