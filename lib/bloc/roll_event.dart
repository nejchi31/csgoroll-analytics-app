import 'package:equatable/equatable.dart';

abstract class RollEvent extends Equatable {
  const RollEvent();
}

class GetRoll extends RollEvent {

  const GetRoll();

  @override
  List<Object> get props => [];
}

class GetDetailedRoll extends RollEvent {
  final String dFrom;
  final String dTo;
  final String method;

  GetDetailedRoll(this.method, this.dFrom, this.dTo);

  @override
  List<Object> get props => [method, dFrom, dTo];
}