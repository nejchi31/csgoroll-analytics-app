import 'package:equatable/equatable.dart';
import 'package:csgoroll_insights_app/data/model/roll.dart';

abstract class RollState extends Equatable {
  const RollState();
}

class RollInitial extends RollState {
  const RollInitial();
  @override
  List<Object> get props => [];
}

class RollLoading extends RollState {
  const RollLoading();
  @override
  List<Object> get props => [];
}

class RollLoaded extends RollState {
  final Roll roll;
  const RollLoaded(this.roll);
  @override
  List<Object> get props => [this.roll];
}

class RollError extends RollState {
  final String message;
  const RollError(this.message);
  @override
  List<Object> get props => [message];
}