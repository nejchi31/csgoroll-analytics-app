import 'package:equatable/equatable.dart';

abstract class CrashEvent extends Equatable {
  const CrashEvent();
}

class GetCrash extends CrashEvent {

  const GetCrash();

  @override
  List<Object> get props => [];
}

class GetDetailedCrash extends CrashEvent {
  final int number;
  final String method;

  GetDetailedCrash(this.number, this.method);

  @override
  List<Object> get props => [number, method];
}