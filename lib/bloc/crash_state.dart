import 'package:equatable/equatable.dart';
import 'package:csgoroll_insights_app/data/model/crash.dart';

abstract class CrashState extends Equatable {
  const CrashState();
}

class CrashInitial extends CrashState {
  const CrashInitial();
  @override
  List<Object> get props => [];
}

class CrashLoading extends CrashState {
  const CrashLoading();
  @override
  List<Object> get props => [];
}

class CrashLoaded extends CrashState {
  final Crash crash;
  const CrashLoaded(this.crash);
  @override
  List<Object> get props => [this.crash];
}

class CrashError extends CrashState {
  final String message;
  const CrashError(this.message);
  @override
  List<Object> get props => [message];
}