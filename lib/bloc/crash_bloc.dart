import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:csgoroll_insights_app/data/crash_repository.dart';
import './bloc.dart';

class CrashBloc extends Bloc<CrashEvent, CrashState> {
  final CrashRepository crashRepository;

  CrashBloc(this.crashRepository) : super(null);

  @override
  CrashState get initialState => CrashInitial();

  @override
  Stream<CrashState> mapEventToState(
    CrashEvent event,
  ) async* {
    yield CrashLoading();
    if (event is GetCrash) {
      try {
        final crash = await crashRepository.fetchCrashAll();
        yield CrashLoaded(crash);
      } on NetworkError {
        yield CrashError("Couldn't fetch Crash. Is the device online?");
      }
    } else if (event is GetDetailedCrash) {
      try {
        print(event.number);
        final crash = await crashRepository.fetchCrashLast(event.number,event.method);
        yield CrashLoaded(crash);
      } on NetworkError {
        yield CrashError("Couldn't fetch Crash. Is the device online?");
      }
    }
  }
}