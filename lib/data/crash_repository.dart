import 'package:http/http.dart' as http;
import 'model/crash.dart';
import 'dart:convert';

abstract class CrashRepository {
  Future<Crash> fetchCrashAll();
  Future<Crash> fetchCrashLast(int number, String method);
}

class UpdateCrashRepository implements CrashRepository {


  @override
  Future<Crash> fetchCrashAll() async{
    print("sadsdad");
    final result = await http.Client().get(Uri.parse("http://localhost:5001/api/csgoroll/crash/?gd=all"));
    print("sadsdad");
    print(result.body);
    if(result.statusCode != 201)
      throw Exception();
    
    return parsedJson(result.body);
      
  }

  @override
  Future<Crash> fetchCrashLast(int number, String method) async{
    print(number);
    print(method);
    String urlApiCallNumber = "http://localhost:5001/api/csgoroll/crash/?gd=lastcrashes&number=" + number.toString();
    String urlApiCallPercentage = "http://localhost:5001/api/csgoroll/crash/?gd=lastcrashes&number=&perc=" + number.toString();

    final result = await http.Client().get(Uri.parse(method=='number'? urlApiCallNumber : urlApiCallPercentage));

    print("sadsdad");
    print(result.body);
    if(result.statusCode != 201)
      throw Exception();
    
    return parsedJson(result.body);
  }
  Crash parsedJson(final response){
    final jsonDecoded = json.decode(response);
    print(jsonDecoded);
    final jsonCrash = jsonDecoded["main"];

    return Crash.fromJson(jsonDecoded);
  }
}

class NetworkError extends Error {}