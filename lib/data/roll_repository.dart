import 'package:http/http.dart' as http;
import 'model/roll.dart';
import 'dart:convert';

abstract class RollRepository {
  Future<Roll> fetchRollAll();
  Future<Roll> fetchRollLast(String method, String dFrom, String dTo);
}

class UpdateRollRepository implements RollRepository {


  @override
  Future<Roll> fetchRollAll() async{
    print("sadsdad");
    final result = await http.Client().get(Uri.parse("http://localhost:5001/api/csgoroll/roll/?gd=all"));
    print("sadsdad");
    print(result.body);
    if(result.statusCode != 201)
      throw Exception();
    
    return parsedJson(result.body);
      
  }

  @override
  Future<Roll> fetchRollLast(String method, String dFrom, String dTo) async{
    print(method);
    String urlApiCallByDate = "http://localhost:5001/api/csgoroll/roll/?gd=byDate&dFrom=" + dFrom;
    String urlApiCallByPeriod = "http://localhost:5001/api/csgoroll/roll/?gd=byPeriod&dFrom=" + dFrom + "&dTo=" + dTo;

    final result = await http.Client().get(Uri.parse(method=='byDate'? urlApiCallByDate : urlApiCallByPeriod));

    print("sadsdad");
    print(result.body);
    if(result.statusCode != 201)
      throw Exception();
    
    return parsedJson(result.body);
  }
  Roll parsedJson(final response){
    final jsonDecoded = json.decode(response);
    print(jsonDecoded);
    final jsonRoll = jsonDecoded["main"];

    return Roll.fromJson(jsonDecoded);
  }
}

class NetworkError extends Error {}