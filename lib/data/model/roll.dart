import 'dart:html';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Roll {
  final int roll_nb;
  final List roll_distribution;
  final List roll_veri;
  final List green_dist;
  final int roll_con_red;
  final int roll_con_black;
  final int roll_con_green;
  final int roll_no_con_red;
  final int roll_no_con_black;
  final int roll_no_con_green;
  
  Roll({
    this.roll_nb,
    this.roll_distribution,
    this.roll_veri,
    this.green_dist,
    this.roll_con_red,
    this.roll_con_black,
    this.roll_con_green,
    this.roll_no_con_red,
    this.roll_no_con_black,
    this.roll_no_con_green,
  });

  @override 
  List<Object> get props => [
    roll_nb, roll_distribution, roll_veri, green_dist, roll_con_red, roll_con_black, roll_con_green, roll_no_con_red, roll_no_con_black, roll_no_con_green
  ];

  Roll.fromJson(Map<String, dynamic> json)
    : roll_nb = json["roll_nb"],
      roll_distribution = json["roll_distribution"],
      roll_veri = json["roll_veri"],
      green_dist = json["green_dist"],
      roll_con_red = json["roll_con_red"],
      roll_con_black = json["roll_con_black"],
      roll_con_green = json["roll_con_green"],
      roll_no_con_red = json["roll_no_con_red"],
      roll_no_con_black = json["roll_no_con_black"],
      roll_no_con_green = json["roll_no_con_green"];
}