import 'dart:html';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Crash {
  final List last5;
  final int gamesPlayed;
  final List biggestCrash;
  final List portportion;
  final List ar_val;
  final List ar_tim;
  
  Crash({
    this.last5,
    this.gamesPlayed,
    this.biggestCrash,
    this.portportion,
    this.ar_val,
    this.ar_tim
  });

  @override 
  List<Object> get props => [
    last5, gamesPlayed, biggestCrash, portportion, ar_val, ar_tim
  ];

  Crash.fromJson(Map<String, dynamic> json)
    : last5 = json["last_5"],
      gamesPlayed = json["games_played"],
      biggestCrash = json["biggest_crash"],
      portportion = json["porpotion"],
      ar_val = json["ar_val"],
      ar_tim = json["ar_tim"];

}