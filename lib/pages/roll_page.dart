import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../data/roll_repository.dart';
import '../bloc/bloc.dart';
import '../data/model/roll.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:humanize/humanize.dart' as humanize;
import 'package:speed_dial_fab/speed_dial_fab.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:intl/intl.dart';

class RollPage extends StatefulWidget {
    const RollPage({
        Key key,
        this.color = const Color(0xffFF0000),
        this.name,
    }) : super(key: key);

    final Color color;
    final String name;
    
    @override
    _RollPageState createState() => _RollPageState();
}

class _RollPageState extends State<RollPage> {
    double _quantity = 3;
    var isDialOpen = ValueNotifier<bool>(false);

    void initState() {
     //getrolldata(context);
    }

    getrolldata(BuildContext context) {
      final rollBloc = BlocProvider.of<RollBloc>(context);
      rollBloc.add(GetRoll());
    }

    @override
    void dispose() {
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      var size = MediaQuery.of(context).size;
      return Scaffold(
        backgroundColor: Color(0xff1d2126),
        bottomNavigationBar: Container(
        height: 48,
        color: Color(0xff31353d),
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Image(image: AssetImage("useCode.png"),)
              ),
            )
          ],
        )
      ),
        body:SafeArea(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[ 
                  Container(
                    height: size.height * .3,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        alignment: Alignment.topCenter,
                        image: AssetImage ('roll.png')
                      )
                    ),
                  ),
                  Positioned(
                    left: 16.0,
                    top: 26.0,
                    child: IconButton(
                      icon: new Icon(Icons.arrow_back, color: Colors.white, size: 32,),
                      onPressed: () => Navigator.of(context).pop(),
                    )
                  ),
                ]
              ),
               BlocBuilder<RollBloc, RollState>(
                  builder: (context, state) {
                    print(state);
                    if (state is RollInitial) {
                      return Text("No data available");
                    } else if (state is RollLoading) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(
                              backgroundColor: Colors.white,
                              valueColor: AlwaysStoppedAnimation(Color(0xff00c74d)),
                              strokeWidth: 10,
                            ),
                        ],
                      );
                    } else if (state is RollLoaded) {
                      print(state.roll);
                      return buildColumnWithData(context, state.roll); //state.Roll);
                    } else if (state is RollError) {
                      return Text("Ups, something is wrong");
                    }
                    else return Text("Ups, something is wrong");
                  },
                ),
            ]
          ),
        ),
        floatingActionButton:SpeedDialFabWidget(
          secondaryIconsList: [
            Icons.radio_button_unchecked,
            Icons.data_usage,
            Icons.data_usage,
          ],
          secondaryIconsText: [
            "All Data",
            "By Date",
            "By Period",
          ],
          secondaryIconsOnPress: [
            () => {
              showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<RollBloc>(context),
                child: ShowDialog()
              ))
            },
            () => {
              showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<RollBloc>(context),
                child: DataByDateDialog(),
              ))
            },
            () => {
              showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<RollBloc>(context),
                child: DataByPeriodDialog(),
              ))
            },
          ],
          secondaryBackgroundColor: Color(0xff00c74d),
          secondaryForegroundColor: Color(0xffffffff),
          primaryBackgroundColor: Color(0xff00c74d),
          primaryForegroundColor: Color(0xffffffff),
        ),
      );  
    }

    Widget buildColumnWithData(BuildContext context, Roll roll) {
        const List imageRollVer = ["roll-red.png", "roll-green.png", "roll-black.png", "roll-rb.png", "roll-bb.png"];
        var size = MediaQuery.of(context).size;
        var images = ["one-green.png", "two-green.png", "three-green.png", "four-green.png"];
        List<Widget> list = List<Widget>();

        for(var i=0; i<images.length; i++){
            list.add(
                Container(
                  height: size.height * 0.08,
                  margin: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Color(0xff31353d),
                    borderRadius: BorderRadius.circular(25),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xff272c33),
                        blurRadius: 8,
                        offset: Offset(1, 2),
                        spreadRadius: 3.0,
                        // Shadow position
                      ),
                    ],
                  ),
                  child:Image(
                    image: AssetImage (images[i]),
                    fit: BoxFit.fitHeight
                  ),
                ),
            );
        }
      
        return Expanded(
          child: SingleChildScrollView(
            child: new Column(
              children: <Widget> [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Games Played',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.05,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_nb).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                    Container(
                      width: size.width * 0.025,
                    ),
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Ad Place',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.05,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text('Sponsored',
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: size.width * 0.025,
                  ),
                  Container(
                    padding: const EdgeInsets.all(12.0),
                    width: size.width * 0.925,
                    child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Roll Distribution',
                                      style: TextStyle(
                                        color:Color(0xff00c74d),
                                        fontSize: 36,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Column(
                                  children: <Widget>[
                                  ListTile(
                                    onTap: ()  {
                                    },
                                    leading: Container(
                                      decoration: BoxDecoration(
                                        color: Color(0xff31353d),
                                        borderRadius: BorderRadius.circular(25),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0xff272c33),
                                            blurRadius: 8,
                                            offset: Offset(1, 2),
                                            spreadRadius: 3.0,
                                            // Shadow position
                                          ),
                                        ],
                                      ),
                                      height: size.height * 0.08,
                                      child: Image(
                                        image: AssetImage("roll-red.png")
                                      ),
                                    ),
                                    title: Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: new LinearPercentIndicator(
                                        animation: true,
                                        lineHeight: 20.0,
                                        animationDuration: 2500,
                                        percent: roll.roll_distribution[0]/(roll.roll_distribution[0]+roll.roll_distribution[1]+roll.roll_distribution[2]),
                                        center: Text((roll.roll_distribution[0]*100/(roll.roll_distribution[0]+roll.roll_distribution[1]+roll.roll_distribution[2])).toStringAsFixed(2) + " %",
                                          style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'ChakraPetch' 
                                          ),
                                        ),
                                        linearStrokeCap: LinearStrokeCap.roundAll,
                                        progressColor: Color(0xffde4c41),
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    onTap: ()  {
                                    },
                                    leading: Container(
                                      decoration: BoxDecoration(
                                        color: Color(0xff31353d),
                                        borderRadius: BorderRadius.circular(25),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0xff272c33),
                                            blurRadius: 8,
                                            offset: Offset(1, 2),
                                            spreadRadius: 3.0,
                                            // Shadow position
                                          ),
                                        ],
                                      ),
                                      height: size.height * 0.08,
                                      child: Image(
                                        image: AssetImage("roll-green.png")
                                      ),
                                    ),
                                    title: Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: new LinearPercentIndicator(
                                        animation: true,
                                        lineHeight: 20.0,
                                        animationDuration: 2500,
                                        percent:roll.roll_distribution[1]/(roll.roll_distribution[0]+roll.roll_distribution[1]+roll.roll_distribution[2]),
                                        center: Text((roll.roll_distribution[1]*100/(roll.roll_distribution[0]+roll.roll_distribution[1]+roll.roll_distribution[2])).toStringAsFixed(2) + " %",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'ChakraPetch' 
                                          ),
                                        ),
                                        linearStrokeCap: LinearStrokeCap.roundAll,
                                        progressColor: Color(0xff00c74d),
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    onTap: ()  {
                                    },
                                    leading: Container(
                                      decoration: BoxDecoration(
                                        color: Color(0xff31353d),
                                        borderRadius: BorderRadius.circular(25),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0xff272c33),
                                            blurRadius: 8,
                                            offset: Offset(1, 2),
                                            spreadRadius: 3.0,
                                            // Shadow position
                                          ),
                                        ],
                                      ),
                                      height: size.height * 0.08,
                                      child: Image(
                                        image: AssetImage("roll-black.png")
                                      ),
                                    ),
                                    title: Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: new LinearPercentIndicator(
                                        animation: true,
                                        lineHeight: 20.0,
                                        animationDuration: 2500,
                                        percent: roll.roll_distribution[2]/(roll.roll_distribution[0]+roll.roll_distribution[1]+roll.roll_distribution[2]),
                                        center: Text((roll.roll_distribution[2]*100/(roll.roll_distribution[0]+roll.roll_distribution[1]+roll.roll_distribution[2])).toStringAsFixed(2) + " %",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'ChakraPetch' 
                                          ),
                                        ),
                                        linearStrokeCap: LinearStrokeCap.roundAll,
                                        progressColor: Color(0xff1d2126),
                                      ),
                                    ),
                                  ),
                                ]),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                  ),
                  SizedBox(
                    height: size.width * 0.025,
                  ),
                  Container(
                    padding: const EdgeInsets.all(12.0),
                    width: size.width * 0.925,
                    child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Distribution on Jackpot chance',
                                      style: TextStyle(
                                        color:Color(0xff00c74d),
                                        fontSize: 18,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                    SizedBox(
                                      height: size.height * 0.05,
                                    )
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Row(
                                  children: [
                                    Container(
                                      width: size.width * 0.40,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: imageRollVer.map((data) => Row(
                                          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              height: size.height * 0.06,
                                              margin: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                color: Color(0xff31353d),
                                                borderRadius: BorderRadius.circular(25),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Color(0xff272c33),
                                                    blurRadius: 8,
                                                    offset: Offset(1, 2),
                                                    spreadRadius: 3.0,
                                                    // Shadow position
                                                  ),
                                                ],
                                              ),
                                              child:Image(
                                                height: size.height * 0.06,
                                                image: AssetImage(data)
                                              ), 
                                            ),
                                          ],
                                        ),
                                      ).toList()
                                      ),
                                    ),
                                    Container(
                                      width: size.width * 0.40,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: roll.roll_distribution.map((data) =>  Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              height: size.height * 0.08,
                                              padding: EdgeInsets.all(4),
                                              child: Text(humanize.intComma(data).toString(),
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'ChakraPetch',
                                                  fontSize: 12,
                                                )
                                              ),
                                            ),
                                          ],
                                        ),
                                      ).toList()
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                  ),
                  SizedBox(
                    height: size.width * 0.025,
                  ),
                  Container(
                    padding: const EdgeInsets.all(12.0),
                    width: size.width * 0.925,
                    child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Text('Greens',
                                        style: TextStyle(
                                          color:Color(0xff00c74d),
                                          fontSize: 26,
                                          fontFamily: 'ChakraPetch' 
                                          ),
                                        textAlign: TextAlign.center),
                                    ),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Row(
                                  children: [
                                    Container(
                                      width: size.width * 0.40,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: list.map((data) => Row(
                                          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            data
                                          ],
                                        ),
                                      ).toList()
                                      ),
                                    ),
                                    Container(
                                      width: size.width * 0.40,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: roll.green_dist.map((data) =>  Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.all(8),
                                              alignment: Alignment.center,
                                              height: size.height * 0.08,
                                              padding: EdgeInsets.all(4),
                                              child: Text(data.toString(),
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'ChakraPetch',
                                                  fontSize: 12,
                                                )
                                              ),
                                            ),
                                          ],
                                        ),
                                      ).toList()
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                  ),
                  SizedBox(
                    height: size.width * 0.025,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Consecutive Red',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.035,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_con_red).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                    Container(
                      width: size.width * 0.025,
                    ),
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('No Consecutive Red',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.035,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_no_con_red).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: size.width * 0.025,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Consecutive Black',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.035,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_con_black).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                    Container(
                      width: size.width * 0.025,
                    ),
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('No Consecutive Black',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.035,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_no_con_black).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: size.width * 0.025,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Consecutive Green',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.035,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_con_green).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                    Container(
                      width: size.width * 0.025,
                    ),
                    Container(
                      width: size.width * 0.45,
                      child: Card(
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40),
                        ),
                        color: Color(0xff31353d),
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                color: Color(0xff272c33),
                                child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('No Consecutive Green',
                                      style: TextStyle(
                                        color: Color(0xff00c74d),
                                        fontSize: size.width * 0.035,
                                        fontFamily: 'ChakraPetch' 
                                        ),
                                      textAlign: TextAlign.center),
                                  ],
                                ),
                              ), 
                            ), 
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(40.0),
                                topRight: const Radius.circular(40.0),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: (size.height*0.15),
                                child: Text(humanize.intComma(roll.roll_no_con_green).toString(),
                                  style: TextStyle(
                                    foreground: Paint()
                                    ..style = PaintingStyle.stroke
                                    ..color = Colors.white,
                                    fontSize: 28,
                                    fontFamily: 'ChakraPetch' 
                                  ),
                                  textAlign: TextAlign.center),
                              ),
                            ), 
                          ]),
                        ),
                      ),
                    ),
                  ]),
                ],
              ),
            ),
        );
      }
}

class ShowDialog extends StatefulWidget {


  @override
  ShowDialogState createState() => ShowDialogState();
}

class ShowDialogState extends State<ShowDialog>{

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext dialogContext) {
        var size = MediaQuery.of(context).size;
        return Dialog(
          child: Container(
            color:Color(0xff1d2126) ,
            height: size.height*.65,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Click to pick All Data',
                  style: TextStyle(
                    foreground: Paint()
                    ..style = PaintingStyle.stroke
                    ..strokeWidth = 2
                    ..color = Colors.white,
                    fontSize: 36,
                    fontFamily: 'ChakraPetch' 
                    ),
                  textAlign: TextAlign.center),
                Container(
                  height: size.height * 0.035,
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                    BlocProvider.of<RollBloc>(context).add(GetRoll());},
                  child: Container(
                    width: size.width * 0.65,
                    child: Image(
                      image: AssetImage ('crash-picker.png'),
                      fit: BoxFit.fitHeight
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
}


class DataByDateDialog extends StatefulWidget {


  @override
  DataByDateDialogState createState() => DataByDateDialogState();
}

class DataByDateDialogState extends State<DataByDateDialog>{

  @override
  void dispose() {
    super.dispose();
  }


  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    String toDate = args.value.toString().substring(0, 10);
    //print(toDate);//%Y-/%m-%d
    Navigator.pop(context);
    BlocProvider.of<RollBloc>(context).add(GetDetailedRoll("byDate", toDate, ""));
  } 

  Widget build(BuildContext dialogContext) {
        var size = MediaQuery.of(context).size;
        return Dialog(
          child: SafeArea(
            child: Container(
              color:Color(0xff1d2126) ,
              height: size.height*.75,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Click to pick All Data',
                    style: TextStyle(
                      foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = 2
                      ..color = Colors.white,
                      fontSize: 36,
                      fontFamily: 'ChakraPetch' 
                      ),
                    textAlign: TextAlign.center),
                  Container(
                    height: size.height * 0.035,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center, //Center Row contents horizontally,
                    crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Container(
                          height: size.height * 0.5, 
                          child: Card(
                            color: Color(0xff31353d),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: SfDateRangePicker(
                                todayHighlightColor: Color(0xff00c74d),
                                selectionColor:Color(0xff00c74d),
                                selectionTextStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold
                                  ),
                                onSelectionChanged: _onSelectionChanged,
                                view: DateRangePickerView.month,
                                selectionMode: DateRangePickerSelectionMode.single,
                                headerStyle: DateRangePickerHeaderStyle(
                                  textStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                yearCellStyle:  DateRangePickerYearCellStyle(
                                  todayTextStyle: TextStyle(
                                    color: Colors.white
                                  ),
                                  textStyle: TextStyle(
                                    color: Colors.white
                                  )
                                ),
                                monthViewSettings:DateRangePickerMonthViewSettings(
                                    showTrailingAndLeadingDates: true
                                    ),
                                monthCellStyle: DateRangePickerMonthCellStyle(
                                  todayTextStyle: TextStyle(
                                    color: Colors.white
                                  ),
                                  textStyle: TextStyle(
                                    color: Colors.white
                                  )
                                ),
                              ),
                            ),
                          )
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      }
}

class DataByPeriodDialog extends StatefulWidget {


  @override
  DataByPeriodDialogState createState() => DataByPeriodDialogState();
}

class DataByPeriodDialogState extends State<DataByPeriodDialog>{

  @override
  void dispose() {
    super.dispose();
  }


  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    if (args.value.startDate != null && args.value.endDate != null) {
      String dFrom = args.value.startDate.toString().substring(0, 10);
      String dTo = args.value.endDate.toString().substring(0, 10);
      //print(toDate);//%Y-/%m-%d
      Navigator.pop(context);
      BlocProvider.of<RollBloc>(context).add(GetDetailedRoll("byDate", dFrom, dTo));
    }
  } 

  Widget build(BuildContext dialogContext) {
    var size = MediaQuery.of(context).size;
    return Dialog(
      child: SafeArea(
        child: Container(
          color:Color(0xff1d2126) ,
          height: size.height*.75,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Click to pick All Data',
                style: TextStyle(
                  foreground: Paint()
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = 2
                  ..color = Colors.white,
                  fontSize: 36,
                  fontFamily: 'ChakraPetch' 
                  ),
                textAlign: TextAlign.center),
              Container(
                height: size.height * 0.035,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center, //Center Row contents horizontally,
                crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Container(
                      height: size.height * 0.5, 
                      child: Card(
                        color: Color(0xff31353d),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: SfDateRangePicker(
                            todayHighlightColor: Color(0xff00c74d),
                            selectionColor:Color(0xff00c74d),
                            endRangeSelectionColor:Color(0xff00c74d),
                            rangeSelectionColor:Color(0xff00c74d),
                            startRangeSelectionColor:Color(0xff00c74d),
                            rangeTextStyle:TextStyle(
                                color: Colors.white
                              ),
                            selectionTextStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold
                              ),
                            onSelectionChanged: _onSelectionChanged,
                            view: DateRangePickerView.month,
                            selectionMode: DateRangePickerSelectionMode.range,
                            headerStyle: DateRangePickerHeaderStyle(
                              textStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            yearCellStyle:  DateRangePickerYearCellStyle(
                              todayTextStyle: TextStyle(
                                color: Colors.white
                              ),
                              textStyle: TextStyle(
                                color: Colors.white
                              )
                            ),
                            monthViewSettings:DateRangePickerMonthViewSettings(
                                showTrailingAndLeadingDates: true
                                ),
                            monthCellStyle: DateRangePickerMonthCellStyle(
                              todayTextStyle: TextStyle(
                                color: Colors.white
                              ),
                              textStyle: TextStyle(
                                color: Colors.white
                              )
                            ),
                          ),
                        ),
                      )
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}