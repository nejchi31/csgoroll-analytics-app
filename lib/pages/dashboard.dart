import 'package:csgoroll_insights_app/data/roll_repository.dart';
import 'package:csgoroll_insights_app/pages/roll_page.dart';
import 'package:csgoroll_insights_app/pages/about_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../data/crash_repository.dart';
import '../bloc/bloc.dart';
import '../data/model/crash.dart';
import 'crash_page.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  void _crashNavigation(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) =>BlocProvider(
      create: (context) => CrashBloc(UpdateCrashRepository()),
      child: CrashPage(),
    )),
    );
  }

  void _rollNavigation(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) =>BlocProvider(
      create: (context) => RollBloc(UpdateRollRepository()),
      child: RollPage(),
    )),
    );
  }

  
  void _aboutNavigation(context) {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (_) => AboutPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xff1d2126),
      bottomNavigationBar: Container(
        height: 48,
        color: Color(0xff31353d),
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Image(image: AssetImage("useCode.png"),)
              ),
            )
          ],
        )
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: size.height * .3,
            decoration: BoxDecoration(
              image: DecorationImage(
                alignment: Alignment.topCenter,
                image: AssetImage ('main.png')
              )
            ),
          ),
          SingleChildScrollView(
            child: GridView.count(
              primary: false,
              crossAxisSpacing: 5.0,
              crossAxisCount: 2,
              shrinkWrap: true,   
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                GestureDetector(
                  onTap: () => {_rollNavigation(context)},
                  child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Image(
                            image: AssetImage ('roll-image.png'),
                            fit: BoxFit.fitHeight
                          ),
                        ),
                        Container(
                          child: Row(
                          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                          crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text('Roll',
                                style: TextStyle(
                                  foreground: Paint()
                                  ..style = PaintingStyle.stroke
                                  ..strokeWidth = 2
                                  ..color = Colors.white,
                                  fontSize: 36,
                                  fontFamily: 'ChakraPetch' 
                                  ),
                                textAlign: TextAlign.center),
                            ],
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => {_crashNavigation(context)},
                  child: Card(
                    color: Color(0xff31353d),
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Image(
                            image: AssetImage ('crash-image.png'),
                            fit: BoxFit.fitHeight
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,   
                            children: <Widget>[
                              Text('Crash',
                                style: TextStyle(
                                  decorationStyle: TextDecorationStyle.wavy,
                                  foreground: Paint()
                                  ..style = PaintingStyle.stroke
                                  ..strokeWidth = 2
                                  ..color = Colors.white,
                                  fontSize: 36,
                                  fontFamily: 'ChakraPetch' 
                                  ),
                                textAlign: TextAlign.center),
                            ],
                          ),
                        ),
                      ]),
                    )
                  ),
                ),
                 GestureDetector(
                  onTap: () => {_aboutNavigation(context)},
                  child: Card(
                    color: Color(0xff31353d),
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Image(
                            image: AssetImage ('about-res.png'),
                            fit: BoxFit.fitWidth
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,   
                            children: <Widget>[
                              Text('About Us',
                                style: TextStyle(
                                  decorationStyle: TextDecorationStyle.wavy,
                                  foreground: Paint()
                                  ..style = PaintingStyle.stroke
                                  ..strokeWidth = 2
                                  ..color = Colors.white,
                                  fontSize: 36,
                                  fontFamily: 'ChakraPetch' 
                                  ),
                                textAlign: TextAlign.center),
                            ],
                          ),
                        ),
                      ]),
                    )
                  ),
                ),
              ]),
          )
        ]
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        /*
        */
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}
