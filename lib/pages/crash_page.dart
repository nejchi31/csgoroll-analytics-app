import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../data/crash_repository.dart';
import '../bloc/bloc.dart';
import '../data/model/crash.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:humanize/humanize.dart' as humanize;
import 'package:speed_dial_fab/speed_dial_fab.dart';


class CrashPage extends StatefulWidget {
    const CrashPage({
        Key key,
        this.color = const Color(0xffFF0000),
        this.name,
    }) : super(key: key);

    final Color color;
    final String name;
    
    @override
    _CrashPageState createState() => _CrashPageState();
}

class _CrashPageState extends State<CrashPage> {
    double _quantity = 3;

    void initState() {
      getcrashdata(context);
    }

    void increaseQty() {
        setState(() { _quantity += 1; });
    }

    @override
    Widget build(BuildContext context) {
      var size = MediaQuery.of(context).size;
      return Scaffold(
        backgroundColor: Color(0xff1d2126),
        bottomNavigationBar: Container(
        height: 48,
        color: Color(0xff31353d),
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Image(image: AssetImage("useCode.png"),)
              ),
            )
          ],
        )
      ),
        body:SafeArea(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[ 
                  Container(
                    height: size.height * .3,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        alignment: Alignment.topCenter,
                        image: AssetImage ('crash.png')
                      )
                    ),
                  ),
                  Positioned(
                    left: 16.0,
                    top: 26.0,
                    child: IconButton(
                      icon: new Icon(Icons.arrow_back, color: Colors.white, size: 32,),
                      onPressed: () => Navigator.of(context).pop(),
                    )
                  ),
                ]
              ),
               BlocBuilder<CrashBloc, CrashState>(
                  builder: (context, state) {
                    print(state);
                    if (state is CrashInitial) {
                      return Text("No data available");
                    } else if (state is CrashLoading) {
                      return CircularProgressIndicator(
                          backgroundColor: Colors.white,
                          valueColor: AlwaysStoppedAnimation(Color(0xff00c74d)),
                          strokeWidth: 10,
                        );
                    } else if (state is CrashLoaded) {
                      print(state.crash.last5);
                      return buildColumnWithData(context, state.crash); //state.crash);
                    } else if (state is CrashError) {
                      return Text("Ups, something is wrong");
                    }
                    else return Text("Ups, something is wrong");
                  },
                ),
            ]
          ),
        ),
        floatingActionButton: /*FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<CrashBloc>(context),
                child: ShowDialog(
                onCallCrashData:_onCallCrashData,
              )
              
        ));}
        ),*/
        SpeedDialFabWidget(
          secondaryIconsList: [
            Icons.radio_button_unchecked,
            Icons.data_usage,
          ],
          secondaryIconsText: [
            "All Data",
            "Last X Data",
          ],
          secondaryIconsOnPress: [
            () => {
              showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<CrashBloc>(context),
                child: ShowDialog()
              ))
            },
            () => {
              showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<CrashBloc>(context),
                child: LastDataDialog()
              ))
            },
          ],
          secondaryBackgroundColor: Color(0xff00c74d),
          secondaryForegroundColor: Color(0xffffffff),
          primaryBackgroundColor: Color(0xff00c74d),
          primaryForegroundColor: Color(0xffffffff),
        ),
      );  
    }


  getcrashdata(BuildContext context) {
    final crashBloc = BlocProvider.of<CrashBloc>(context);
    crashBloc.add(GetCrash());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildColumnWithData(BuildContext context, Crash crash) {
    var size = MediaQuery.of(context).size;
    return Expanded(
      child: SingleChildScrollView(
        child: new Column(
          children: <Widget> [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Container(
                  width: size.width * 0.45,
                  child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            color: Color(0xff272c33),
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Games Played',
                                  style: TextStyle(
                                    color: Color(0xff00c74d),
                                    fontSize: size.width * 0.05,
                                    fontFamily: 'ChakraPetch' 
                                    ),
                                  textAlign: TextAlign.center),
                              ],
                            ),
                          ), 
                        ), 
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            height: (size.height*0.15),
                            child: Text(humanize.intComma(crash.gamesPlayed).toString(),
                              style: TextStyle(
                                foreground: Paint()
                                ..style = PaintingStyle.stroke
                                ..color = Colors.white,
                                fontSize: 28,
                                fontFamily: 'ChakraPetch' 
                              ),
                              textAlign: TextAlign.center),
                          ),
                        ), 
                      ]),
                    ),
                  ),
                ),
                Container(
                  width: size.width * 0.025,
                ),
                Container(
                  width: size.width * 0.45,
                  child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            color: Color(0xff272c33),
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Ad Place',
                                  style: TextStyle(
                                    color: Color(0xff00c74d),
                                    fontSize: size.width * 0.05,
                                    fontFamily: 'ChakraPetch' 
                                    ),
                                  textAlign: TextAlign.center),
                              ],
                            ),
                          ), 
                        ), 
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            height: (size.height*0.15),
                            child: Text('Sponsored',
                              style: TextStyle(
                                foreground: Paint()
                                ..style = PaintingStyle.stroke
                                ..color = Colors.white,
                                fontSize: 28,
                                fontFamily: 'ChakraPetch' 
                              ),
                              textAlign: TextAlign.center),
                          ),
                        ), 
                      ]),
                    ),
                  ),
                ),
              ]),
              SizedBox(
                height: size.width * 0.025,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Container(
                  width: size.width * 0.45, 
                  child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: Column(
                      children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(40.0),
                          topRight: const Radius.circular(40.0),
                        ),
                        child: Container(
                          color: Color(0xff272c33),
                          child: Row(
                          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                          crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text('Biggest Crash',
                                style: TextStyle(
                                  color: Color(0xff00c74d),
                                  fontSize: size.width * 0.05,
                                  fontFamily: 'ChakraPetch' 
                                  ),
                                textAlign: TextAlign.center),
                            ],
                          ),
                        ), 
                      ), 
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(40.0),
                          topRight: const Radius.circular(40.0),
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          height: (size.height*0.15),
                          child: Text(crash.biggestCrash[4].toString(),
                            style: TextStyle(
                              foreground: Paint()
                              ..style = PaintingStyle.stroke
                              ..color = Colors.white,
                              fontSize: 28,
                              fontFamily: 'ChakraPetch' 
                            ),
                            textAlign: TextAlign.center),
                        ),
                      ), 
                    ]),
                  ),
                ),
                Container(
                  width: size.width * 0.025,
                ),
                Container(
                  width: size.width * 0.45,
                  child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            color: Color(0xff272c33),
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text('Top 5 Crashes',
                                    style: TextStyle(
                                      color: Color(0xff00c74d),
                                      fontSize: size.width * 0.05,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    textAlign: TextAlign.center),
                                ),
                              ],
                            ),
                          ), 
                        ), 
                        Container(
                          height: (size.height*0.15),
                          child: Column(
                            children: crash.biggestCrash.map((data) => Container(
                              padding: EdgeInsets.all(4),
                              child: Text(data.toString(),
                                style: TextStyle(
                                  foreground: Paint()
                                  ..style = PaintingStyle.stroke
                                  ..color = Colors.white,
                                  fontSize: 14,
                                  fontFamily: 'ChakraPetch' 
                                  ),
                                textAlign: TextAlign.center
                              ),
                            )).toList()
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
              ]),
              SizedBox(
                height: size.width * 0.025,
              ),
              Container(
                padding: const EdgeInsets.all(12.0),
                width: size.width * 0.925,
                child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            color: Color(0xff272c33),
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Arrangement',
                                  style: TextStyle(
                                    color:Color(0xff00c74d),
                                    fontSize: 36,
                                    fontFamily: 'ChakraPetch' 
                                    ),
                                  textAlign: TextAlign.center),
                              ],
                            ),
                          ), 
                        ), 
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              children: <Widget>[
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("1-2",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[0]/100,
                                    center: Text(crash.portportion[0].toString() + " %",
                                      style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("2-3",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent:crash.portportion[1]/100,
                                    center: Text(crash.portportion[1].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("3-4",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[2]/100,
                                    center: Text(crash.portportion[2].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("4-5",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[3]/100,
                                    center: Text(crash.portportion[3].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                /*
                                leading: Image(
                                  image: AssetImage ('roll-rb.png'),
                                  fit: BoxFit.fitHeight
                                ),*/
                                leading: Text("5-10",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[4]/100,
                                    center: Text(crash.portportion[4].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("10-100",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[5]/100,
                                    center: Text(crash.portportion[5].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("100-1000",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[6]/100,
                                    center: Text(crash.portportion[6].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                              ListTile(
                                onTap: ()  {
                                },
                                leading: Text("1000-100000",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'ChakraPetch',
                                    fontSize: 12,
                                  )
                                ),
                                title: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: new LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 20.0,
                                    animationDuration: 2500,
                                    percent: crash.portportion[7]/100,
                                    center: Text(crash.portportion[7].toString() + " %",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'ChakraPetch' 
                                      ),
                                    ),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Color(0xffde4c41),
                                  ),
                                ),
                              ),
                            ]),
                          ),
                        ), 
                      ]),
                    ),
                  ),
              ),
              SizedBox(
                height: size.width * 0.025,
              ),
              Container(
                padding: const EdgeInsets.all(12.0),
                width: size.width * 0.925,
                child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Color(0xff31353d),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            color: Color(0xff272c33),
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                            crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Most Frequent Crashes [Crash/Times]',
                                  style: TextStyle(
                                    color:Color(0xff00c74d),
                                    fontSize: 18,
                                    fontFamily: 'ChakraPetch' 
                                    ),
                                  textAlign: TextAlign.center),
                              ],
                            ),
                          ), 
                        ), 
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                          child: Container(
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Container(
                                  width: size.width * 0.40,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: crash.ar_val.map((data) =>Container(
                                      padding: EdgeInsets.all(4),
                                      child: Text(data.toString(),
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'ChakraPetch',
                                            fontSize: 12,
                                          )
                                        ),
                                    ),
                                  ).toList()
                                  ),
                                ),
                                Container(
                                  width: size.width * 0.40,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: crash.ar_tim.map((data) =>  Container(
                                      padding: EdgeInsets.all(4),
                                      child: Text(humanize.intComma(data).toString(),
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'ChakraPetch',
                                            fontSize: 12,
                                          )
                                        ),
                                    ),
                                  ).toList()
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ), 
                      ]),
                    ),
                  ),
              ),
              SizedBox(
                height: size.width * 0.025,
              ),
              Container(
                width: size.width * 0.90,
                child: Card(
                  elevation: 20,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  color: Color(0xff31353d),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(40.0),
                          topRight: const Radius.circular(40.0),
                        ),
                        child: Container(
                          color: Color(0xff272c33),
                          child: Row(
                          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                          crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text('Last Crashes',
                                style: TextStyle(
                                  color: Color(0xff00c74d),
                                  fontSize: 36,
                                  fontFamily: 'ChakraPetch' 
                                  ),
                                textAlign: TextAlign.center),
                            ],
                          ),
                        ), 
                      ), 
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(40.0),
                          topRight: const Radius.circular(40.0),
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          height: (size.height*0.15),
                          child: Column(
                            children: [
                              Expanded(
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: crash.last5.map((data) => Container(
                                      padding: EdgeInsets.all(8),
                                      child: Text(data.toString(),
                                          style: TextStyle(
                                            foreground: Paint()
                                            ..style = PaintingStyle.stroke
                                            ..color = Colors.white,
                                            fontSize: 28,
                                            fontFamily: 'ChakraPetch' 
                                            ),
                                          textAlign: TextAlign.center
                                        ),
                                    )).toList(),     
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ), 
                    ]),
                  ),
                ),
              ),
            ],
          ),
        ),
    );
  }
}

class ShowDialog extends StatefulWidget {


  @override
  ShowDialogState createState() => ShowDialogState();
}

class ShowDialogState extends State<ShowDialog>{

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext dialogContext) {
        var size = MediaQuery.of(context).size;
        return Dialog(
          child: Container(
            color:Color(0xff1d2126) ,
            height: size.height*.65,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Click to pick All Data',
                  style: TextStyle(
                    foreground: Paint()
                    ..style = PaintingStyle.stroke
                    ..strokeWidth = 2
                    ..color = Colors.white,
                    fontSize: 36,
                    fontFamily: 'ChakraPetch' 
                    ),
                  textAlign: TextAlign.center),
                Container(
                  height: size.height * 0.035,
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                    BlocProvider.of<CrashBloc>(context).add(GetCrash());},
                  child: Container(
                    width: size.width * 0.65,
                    child: Image(
                      image: AssetImage ('crash-picker.png'),
                      fit: BoxFit.fitHeight
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
}


class LastDataDialog extends StatefulWidget {


  @override
  LastDataDialogState createState() => LastDataDialogState();
}

class LastDataDialogState extends State<LastDataDialog>{
  String method = "";
  int numberLast = 0;

  @override
  void dispose() {
    super.dispose();
  }

  List<bool> _selection = List.generate(2, (_)=>false);

  Widget build(BuildContext dialogContext) {
        var size = MediaQuery.of(context).size;
        return Dialog(
          child: Container(
            color:Color(0xff1d2126) ,
            height: size.height*.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Click to pick All Data',
                  style: TextStyle(
                    foreground: Paint()
                    ..style = PaintingStyle.stroke
                    ..strokeWidth = 2
                    ..color = Colors.white,
                    fontSize: 36,
                    fontFamily: 'ChakraPetch' 
                    ),
                  textAlign: TextAlign.center),
                Container(
                  height: size.height * 0.035,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center, //Center Row contents horizontally,
                  crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,
                  children: [
                    Container(
                      height: size.height * 0.05,
                      child: ToggleButtons(
                        children: [
                          Image(image: AssetImage("percentage-pick.png")),
                          Image(image: AssetImage("number-picker.png"))
                        ],
                        isSelected: _selection,
                        onPressed: (int index) {
                          setState(() {
                            for(var i=0; i<_selection.length; i++){
                              if(i==index){
                                _selection[i] = true;
                              }else{
                                _selection[i] = false;
                              }
                            }  
                            if(index==0){
                              method = "percentage";
                            }else{
                              method = "number";
                            }                   
                          });},
                        selectedBorderColor: Color(0xff00c74d),
                      ),
                    ), 
                  ],  
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center, //Center Row contents horizontally,
                  crossAxisAlignment: CrossAxisAlignment.center, //Center Row contents vertically,
                  children: [
                  Container(
                    alignment: Alignment.center,
                    height: size.height * 0.1,
                    width: size.width * 0.5,
                    child: Row(
                      children: [
                        Expanded (
                          child: TextFormField(
                            obscureText: false,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(color: Colors.white),
                              hintText: 'Dont use decimals',
                              hintStyle: TextStyle(color:Colors.white),
                              focusedBorder:OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xffffffff),
                                  width: 3.0
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xff00c74d),
                                  width: 2.0
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xff00c74d), width: 3.0),
                              ),
                              labelText: 'Range',
                              focusColor: Color(0xff00c74d)
                            ),
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Color(0xff00c74d)),
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please select range';
                              }
                              return null;
                            },
                            onChanged: (value){
                              int temp;
                              try{
                                temp = int.parse(value);
                                if(method=="percentage" && temp>100){
                                  setState(() {
                                    numberLast = numberLast;                          
                                  });
                                }
                                setState(() {
                                  numberLast = temp;                          
                                });
                              }catch (err){
                                return err;
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],),
                Container(
                  height: size.height * 0.015,
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                    BlocProvider.of<CrashBloc>(context).add(GetDetailedCrash(numberLast, method));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xff00c74d),
                      border: Border.all(
                        color: const Color(0xff272c33),
                        width: 5,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    height: size.height * 0.15,
                    width: size.height * 0.15,
                    child: Image(
                      image: AssetImage ('crash-picker-last.png'),
                      fit: BoxFit.fitHeight
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
}