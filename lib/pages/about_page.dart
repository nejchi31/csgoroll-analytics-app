import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class AboutPage extends StatefulWidget {
    const AboutPage({
        Key key,
        this.color = const Color(0xffFF0000),
        this.name,
    }) : super(key: key);

    final Color color;
    final String name;
    
    @override
    _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
    double _quantity = 3;
    var isDialOpen = ValueNotifier<bool>(false);

    void initState() {
     //getrolldata(context);
    }


    @override
    void dispose() {
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      var size = MediaQuery.of(context).size;
      return Scaffold(
        backgroundColor: Color(0xff1d2126),
        bottomNavigationBar: Container(
        height: 48,
        color: Color(0xff31353d),
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Image(image: AssetImage("useCode.png"),)
              ),
            )
          ],
        )
      ),
        body:SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[ 
                    Container(
                      height: size.height * .3,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          alignment: Alignment.topCenter,
                          image: AssetImage ('about-nav.png')
                        )
                      ),
                    ),
                    Positioned(
                      left: 16.0,
                      top: 26.0,
                      child: IconButton(
                        icon: new Icon(Icons.arrow_back, color: Colors.white, size: 32,),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    ),
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(size.width*0.05, size.height * 0.3, 8, size.width*0.05),
                          child: Column(children: [
                            Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: Text(
                                "Hello, we are nejchi31 and iceeu. We really enjoy to be part of CSGORoll community so we are creating videos, blogs, etc for you. We try to be active and productive as possible. If you want to support us please check out our platforms." ,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 26,
                                  fontFamily: 'ChakraPetch' 
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8.0, 2 , 24,2),
                              child: Divider(
                                color: Colors.white,
                                thickness: 2,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(12),
                              child: Row(
                                children: [
                                  Container(
                                    child: IconButton(
                                      iconSize: size.width * 0.25,
                                      icon: Image(image: AssetImage("youtube.png"),
                                      ),
                                      onPressed: () async {
                                        const url = 'https://www.youtube.com/channel/UC3PgEiqPSw99D45ad94y2fw';
                                        if (await canLaunch(url)) {
                                          await launch(url);
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Container(
                                     child: IconButton(
                                      iconSize: size.width * 0.25,
                                      icon: Image(image: AssetImage("webpage.png"),
                                      ),
                                      onPressed: () async {
                                        const url = 'https://iceeeu.com/';
                                        if (await canLaunch(url)) {
                                          await launch(url);
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Container(
                                     child: IconButton(
                                      iconSize: size.width * 0.25,
                                      icon: Image(image: AssetImage("hypedrop.png"),
                                      ),
                                      onPressed: () async {
                                        const url = 'https://hypedrop.com/r/ICEE123';
                                        if (await canLaunch(url)) {
                                          await launch(url);
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      },
                                    ),
                                  ),
                                ],
                            ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: Text(
                                "To support us please use our code for 5% deposit on CSGORoll.",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 26,
                                  fontFamily: 'ChakraPetch' 
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(32,0,32.0,0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 32,
                                  ),
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                          Container(
                                            //color: Colors.white,
                                            child: Text(
                                              "icee123",
                                              style: TextStyle(
                                                color: Color(0xff00c74d),
                                                fontSize: size.width * 0.05,
                                                fontFamily: 'ChakraPetch' 
                                                ),
                                              textAlign: TextAlign.center),
                                          )
                                        ],),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                          Text(
                                            "nejchi31",
                                            style: TextStyle(
                                              color: Color(0xff00c74d),
                                              fontSize: size.width * 0.05,
                                              fontFamily: 'ChakraPetch' 
                                              ),
                                            textAlign: TextAlign.center
                                          ),
                                        ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center ,//Center Row contents horizontally,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Text(
                                    "To report the bug please DM me on nejchi31@gmail.com",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 26,
                                      fontFamily: 'ChakraPetch' 
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ]
                ),  
              ]
            ),
          ),
        ),
      );  
    }

    
}


