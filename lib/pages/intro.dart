import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../data/crash_repository.dart';
import '../bloc/bloc.dart';
import 'dashboard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:after_layout/after_layout.dart';
import 'package:introduction_screen/introduction_screen.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      color: Colors.blue,
      title: 'CSGORoll Analyzer',
      home: new Splash(),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  SplashState createState() => new SplashState();
}

class SplashState extends State<Splash> with AfterLayoutMixin<Splash> {
  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);

    if (_seen) {
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => new Home()));
    } else {
      await prefs.setBool('seen', true);
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => new IntroScreen()));
    }
  }

  @override
  void afterFirstLayout(BuildContext context) => checkFirstSeen();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Text('Loading...'),
      ),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CrashBloc(UpdateCrashRepository()),
      child: Dashboard(),
    );
  }
}

class IntroScreen extends StatefulWidget  {
   @override
  IntroState createState() => IntroState();
}

class IntroState extends State<IntroScreen> { 
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => Dashboard()
      ),
    );
  }

  Widget _buildFullscrenImage() {
    return Container(
      margin: const EdgeInsets.all(44.0),
      child: Image.asset(
        'intro-1.png',
        fit: BoxFit.fitHeight,
        height: double.infinity,
        width: double.infinity,
        alignment: Alignment.center,
      ),
    );
  }

  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset(assetName, width: width);
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0, color: Colors.white,fontFamily: 'ChakraPetch' );

    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700, color:Color(0xff00c74d)),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Color(0xff1d2126),
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: Color(0xff1d2126),
      color: Color(0xff1d2126),
      globalFooter: SizedBox(
        width: double.infinity,
        height: 60,
        child: Row(
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: const Text(
                  'Introduction',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.white,fontFamily: 'ChakraPetch' ),
                ),
              ),
            ),
          ],
        ),
      ),
      pages: [
        PageViewModel(
          title: "",
          image: _buildFullscrenImage(),
          body:
              "",
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Welcome Rollers",
          body:
              "Thank you for downloading an app. Use of this app is totaly FREE. \n Description: Application enables you to look at historical data of Roll and Crash on CSGORoll. ",
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "How does it works?",
          body:
              "Application gets data from server where data is handled by scripts. Script are freely available on my Github account: nejchi31. After API call you will be served some interesting data.",
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "About",
          body:
              "The development of this app has been made totally in my free time. Although my job takes a lot of energy I wanted to give you something. Not even in my mind I have thoughts to makemoney with app. However, I would really appreciate if you would use my code: nejchi31 to show be support of doing an app.",
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Iceeu & nejchi31 Gang",
          body:
              "You can follow us on many channel. Please take a minute and navigate to Gang section and show us some love :). Thank you, with love <3.",
          
          decoration: pageDecoration.copyWith(
            contentMargin: const EdgeInsets.symmetric(horizontal: 16),
            fullScreen: true,
            bodyFlex: 2,
            imageFlex: 3,
          ),
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: false,
      skipFlex: 0,
      nextFlex: 0,
      //rtl: true, // Display as right-to-left
      skip: const Text('Skip'),
      next: const Icon(Icons.arrow_forward),
      done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600, color: Color(0xff00c74d))),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: const EdgeInsets.all(12.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFF00c74d),
        activeColor: Color(0xff00c74d),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        color: Color(0xFF1d2126),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}